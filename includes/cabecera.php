<?php require_once 'conexion.php'; ?>
<?php require_once 'includes/helpers.php';?>
<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Blog de video juegos</title>
        <link href="assets/css/estilos.css" rel="stylesheet"/>
    </head>
    <body>
        <!--CABECERA -->
        <header id="header">
            <!--logo-->
            <div id="logo">
                <a href="index.php">
                    Blog de Videojuegos
                </a>
            </div>
            <!--MENU-->
            
            
            <nav id="menu">
                <ul>
                    <li>
                        <a href="index.php">Inicio</a>
                    </li>
                    <?php 
                        $categorias = conseguirCategorias($db);
                        if(!empty($categorias)):
                            while($categoria = mysqli_fetch_assoc($categorias)):
                    ?>
                                <li>
                                <a href="categoria.php?id=<?=$categoria['id']?>"><?= $categoria['nombre']?></a>
                                </li>
                    <?php 
                            endwhile;
                        endif;
                    ?> 
                    <li>
                        <a href="">Sobre mí</a>
                    </li>
                    <li>
                        <a href="">Contacto</a>
                    </li>
                </ul>
            </nav>
        </header>

        <div id="contenedor">