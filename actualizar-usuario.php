<?php
if(isset($_POST)){// si existe el boton con nombre submit

    ////conexion a la base de datos
    require_once 'includes/conexion.php';

    
    //recojer los valores del formulario de registro
    $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db,$_POST['nombre']) : false;//operador ternario
    $apellidos = isset($_POST['apellidos']) ? mysqli_real_escape_string($db,$_POST['apellidos']) : false;
    $email = isset($_POST['email']) ? mysqli_real_escape_string($db, trim($_POST['email'])) : false;
    /*$password = isset($_POST['password']) ? mysqli_real_escape_string($db,$_POST['password']) : false;*/
    
    
    //Array de errores
    $errores = array();
    //validar datos aantes de guardarlos en la base de datos
    //validar campo nombre
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_validado = true;
    }else{
        $nombre_validado = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    
    //validar compo apellidos
    if(!empty($apellidos) && !is_numeric($apellidos) && !preg_match("/[0-9]/", $apellidos)){
        $apellidos_validado = true;
    }else{
        $apellidos_validado = false;
        $errores['apellidos'] = "Los apellidos no son validos";
    }
    
    //validar campo email
    if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)){
        $email_validado = true;
    }else{
        $email_validado = false;
        $errores['email'] = "El email es invalido";
    }
    
    /*//validar compo contraseña
    if(!empty($password)){
        $password_validado = true;
    }else{
        $password_validado = false;
        $errores['password'] = "La contraseña esta vacía";
    }*/
    
    $guardar_usuario = false;
    if (count($errores) == 0) {
        $usuario= $_SESSION['usuario'];
        $guardar_usuario = true;
        //cifrar la contraseña
        /*$password_segura = password_hash($password, PASSWORD_BCRYPT, ['cost'=>4]);//cifrar contraseña*/
        
        //comprobar si el email ya existe
        $sql = "SELECT id, email FROM usuarios WHERE email = '$email'";
        $isset_email = mysqli_query($db, $sql);
        $isset_user = mysqli_fetch_assoc($isset_email);
        
        if ($isset_user['id'] == $usuario['id'] || empty($isset_user)) {
        //Actualizar usuario en la tabla usuarios de la bbdd 
            
            $sql = "UPDATE usuarios SET ".
                   "nombre = '$nombre', ".
                   "apellidos = '$apellidos', ".
                   "email = '$email' ".
                   "WHERE id = ".$usuario['id'];// consulta de update
            $guardar = mysqli_query($db, $sql);


            if($guardar){
                $_SESSION['usuario']['nombre'] = $nombre;
                $_SESSION['usuario']['apellidos'] = $apellidos;
                $_SESSION['usuario']['email'] = $email;
                $_SESSION['completado'] = "Tus datos se han actualizado con exito";
            }else{
                $_SESSION['errores']['general'] = "Fallo al actualizar tus datos!!";
            }
        }else{
            $_SESSION['errores']['general'] = "El email ya existe";
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}
header('Location: mis-datos.php');
