<?php
//iniciar la conexion a la base de datos
require_once 'includes/conexion.php';

//recojer los datos del formulario
if(isset($_POST)){
    //borrar error antiguo
    if (isset($_SESSION['error_login'])) {
        /*session_unset*/unset($_SESSION['error_login']); 
    }
    //recojer los datos del formulario
    $email = trim($_POST['email']);
    $password = $_POST['password'];
    
    //Consulta para comprobar las credenciales del usuario
    $sql = "SELECT * FROM usuarios WHERE email = '$email'";
    $login = mysqli_query($db, $sql);
    
    if ($login && mysqli_num_rows($login) == 1 ) {
        $usuario = mysqli_fetch_assoc($login);

        //comporbar la contrseña /cifrar 
        $verify = password_verify($password, $usuario['password']);
        
        if ($verify) {
            
            //Utilizar una session para guardar los datos del usuario logeado
            $_SESSION['usuario']=$usuario;
            

        }else{
            //Si algo falla enviar uns session con el fallo 
            $_SESSION['error_login']= "Login incorrecto";
        }
        
    }else{
        //mensaje de error
        $_SESSION['error_login']= "Login incorrecto";
    }
}
//Redirigir al index.php
header('Location: Index.php');
